provider "aws" {
  region     = "eu-west-1"
}

resource "aws_instance" "VIVAtest" {
  ami = "ami-060e3b9873933dc14"
  instance_type = "t2.micro"
  key_name = "viva_test"
  vpc_security_group_ids = ["sg-71764c36"]
  private_ip = "172.31.42.60"
}
